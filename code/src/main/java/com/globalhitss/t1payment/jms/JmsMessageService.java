package com.globalhitss.t1payment.jms;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.exception.AppWsException;



public interface JmsMessageService {
	
	void sendMessageToGateway(GatewayRequestMessage requestMessage);
	GatewayResponseMessage waitTillResponse(String requestId, long waitTime) throws AppWsException;
	void registerToWait(GatewayResponseMessage responseMessage);
	boolean setResponse(String responseMessageJson);
}
