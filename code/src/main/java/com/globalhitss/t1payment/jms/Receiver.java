package com.globalhitss.t1payment.jms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.model.SystemIdModel;

@Component
@EnableJms
public class Receiver {

	@Autowired
	private JmsMessageService jmsMessageService;

	private static final Log LOG = LogFactory.getLog(Receiver.class);

	/**
	 * This listen a queue and put the message in map to recovery later..
	 */
	@JmsListener(destination = "#{systemIdModel.responseQueue}", containerFactory="myFactory")
	public void receiveMessage(String responseMessage) throws AppWsException {
		try {
			LOG.info("Received object: " +  responseMessage);
			jmsMessageService.setResponse(responseMessage);
		} catch (Exception e) {
			throw new AppWsException("Error durante la recepcion del mensaje", 1001, "Error: " + e.getMessage());
		}

	}
}
