package com.globalhitss.t1payment.jms.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.converter.JsonResponseToObject;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.jms.JmsMessageService;
import com.globalhitss.t1payment.model.SystemIdModel;

@Service("jmsMessageService")
public class JmsMessageServiceImpl implements JmsMessageService {

	private static final Log LOG = LogFactory.getLog(JmsMessageServiceImpl.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private SystemIdModel systemId;

	@Autowired
	private JsonResponseToObject jsonToObject;

	private Map<String, GatewayResponseMessage> map;

	@PostConstruct
	public void init() {
		LOG.info("Map completed");
		map = new ConcurrentHashMap<>(256, 0.5F, 64);
	}

	@Override
	public void sendMessageToGateway(GatewayRequestMessage requestMessage) {

		LOG.info("Sending requestMessage via -" + systemId.getRequestQueue() + "-");
		jmsTemplate.convertAndSend(systemId.getRequestQueue(), requestMessage);
	}

	@Override
	public GatewayResponseMessage waitTillResponse(String requestId, long waitTime) throws AppWsException {
		GatewayResponseMessage responseMessage = map.get(requestId);
		if (responseMessage == null) {
			throw new AppWsException(
					"NPE, response not registered to wait. You should call registerToWait before waitResponse ", 999400,
					"response not registered to wait");
		}
		if (responseMessage.isReceived()) {
			LOG.info("Message Received before wait");
			return map.remove(requestId);
		}
		try {
			synchronized (responseMessage) {
				LOG.info("Waiting response..");
				responseMessage.wait(waitTime != 0L ? waitTime : 10000L);
			}
		} catch (InterruptedException e) {
			LOG.error("InterruptedException - Mientras se esperaba la respuesta del Core Transfer: MessageId:"
					+ requestId, e);
			map.remove(requestId);
			return null;
		}
		return map.remove(requestId);
	}

	@Override
	public void registerToWait(GatewayResponseMessage responseMessage) {
		LOG.info("Message to register: " + responseMessage.toString());
		map.put(String.valueOf(responseMessage.getMessageId()), responseMessage);
	}

	@Override
	public boolean setResponse(String responseMessageJson) {
		String messageId = null;
		try {
			GatewayResponseMessage responseMessage = jsonToObject.jsonToGatewayResponseMessage(responseMessageJson);
			if (responseMessage != null) {
				messageId = String.valueOf(responseMessage.getMessageId());

				GatewayResponseMessage responseMessageInMap = map.get(messageId);

				responseMessageInMap.setCode(responseMessage.getCode());
				responseMessageInMap.setFail(responseMessage.isFail());
				responseMessageInMap.setMessage(responseMessage.getMessage());
				responseMessageInMap.setMessageType(responseMessage.getMessageType());
				responseMessageInMap.setMsisdn(responseMessage.getMsisdn());
				responseMessageInMap.setParams(responseMessage.getParams());
				responseMessageInMap.setReceived(responseMessage.isReceived());
				responseMessageInMap.setSubject(responseMessage.getSubject());
				responseMessageInMap.setSuccess(responseMessage.isSuccess());
				responseMessageInMap.setTypeMessage(responseMessage.getTypeMessage());

				synchronized (responseMessageInMap) {
					responseMessageInMap.notify();
				}
				return true;
			}
			LOG.warn((new StringBuilder()).append("WebRequestContext es null para el ID [").append(messageId)
					.append("]").toString());
			return false;
		} catch (Exception e) {
			LOG.error("ERROR: the json response from gateway is invalid", e);
			return false;
		}
	}
}
