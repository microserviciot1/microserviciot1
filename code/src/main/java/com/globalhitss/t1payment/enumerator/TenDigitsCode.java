package com.globalhitss.t1payment.enumerator;

public enum TenDigitsCode {
	ATT("306"), ANA_SEGUROS("303");
	String code;

	TenDigitsCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static boolean existCode(String value) {
		boolean exist = false;
		if (value != null) {
			for (TenDigitsCode typeEnum : TenDigitsCode.values()) {
				if (typeEnum.getCode().equals(value)) {
					exist = true;
					break;
				}
			}
		}

		return exist;
	}
}
