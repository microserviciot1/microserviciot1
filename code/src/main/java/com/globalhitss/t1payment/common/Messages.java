package com.globalhitss.t1payment.common;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component("messagesClaro")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class Messages {
	private static final Log LOG = LogFactory.getLog(Messages.class);
	
	public static final String PREF_CODE="code.";
	private MessageSourceAccessor accesor;
	private Map<String, String> allValues = new HashMap<String, String>();

	@Autowired
	Environment env;

	PropertyPlaceholderConfigurer properties;

	@PostConstruct
	private void init() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		LOG.info("GENERATING accesor..");
		accesor = new MessageSourceAccessor(messageSource);
		loadAllMessages();
	}

	private void loadAllMessages() {

		Properties props = new Properties();
		try {
			props.load(getClass().getResourceAsStream("/messages.properties"));
			for (Enumeration<?> e = props.propertyNames(); e.hasMoreElements();) {
				String key = (String) e.nextElement();
				String value = (String) props.getProperty(key);
				allValues.put(key, value);
			}
		} catch (Exception e) {
			LOG.error("Error, messages.properties not found in the classpath: " + e.getMessage(), e);
		}
	}

	public String get(String code) {
		try {
			return accesor.getMessage(code);
		} catch (NoSuchMessageException e) {
			LOG.error("Message not found in messages.properties file " + e.getMessage(), e);
			return "";
		}
	}

	public Map<String, String> getAllMessages() {
		return this.allValues;
	}
}
