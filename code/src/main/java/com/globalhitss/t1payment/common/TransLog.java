package com.globalhitss.t1payment.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;



@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TransLog {

	public String operationName() default "";
	
	public String successCode() default "0";

	public String errorDescription() default "";

	public String errorCode() default "";

}