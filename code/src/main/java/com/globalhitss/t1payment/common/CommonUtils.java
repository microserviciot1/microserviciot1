package com.globalhitss.t1payment.common;

import java.text.ParseException;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.MaskFormatter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.globalhitss.t1payment.model.SystemIdModel;
import com.globalhitss.t1payment.util.SystemUtil;



@Component
public class CommonUtils {
	
	@Autowired
	private SystemIdModel systemIdModel;

	private static final Log LOG = LogFactory.getLog(CommonUtils.class);

	public long getMessageId() {
		LOG.info("SYSTEM_ID: " + systemIdModel.getSystemId());
		return SystemUtil.getCorrelationId(Short.parseShort(systemIdModel.getSystemId()));
	}

	public Integer getTransOrigin(Integer typeOrigin) {
		if (!typeOrigin.equals(0)) {
			switch (typeOrigin) {
			case 2:
				LOG.info("La transaccionn tiene origen tipo: " + typeOrigin + " - Teclado Transfer ");
				break;
			default:
				LOG.info("Origen desconocido : " + typeOrigin);
				break;
			}
		}
		return typeOrigin;
	}
	
	public void printParameters(HttpServletRequest request) {
		LOG.info("----PARAMETERS-----");
		LOG.info("SESSION ID:" +  request.getSession().getId());
		Enumeration<String> enParams = request.getParameterNames();
		while (enParams.hasMoreElements()) {
			String paramName = (String) enParams.nextElement();
			if (!paramName.startsWith("oauth") && !paramName.startsWith("msisdnList"))
				LOG.info(paramName + " ::: " + request.getParameter(paramName));
		}
		LOG.info("--------------------");
	}
	
	public String formatFolio(String folio) {
		LOG.info("Formatting the folio");
		if(folio != null) {
			try {
				MaskFormatter formatter = new MaskFormatter("**** **** ****");
				formatter.setValueContainsLiteralCharacters(false);
				folio = formatter.valueToString(folio).trim();
			}catch(ParseException e) {
				LOG.error("Error parsing folio " + folio );
			}
		}
		return folio;
	}
}
