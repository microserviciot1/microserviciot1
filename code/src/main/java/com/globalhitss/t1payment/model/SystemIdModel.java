package com.globalhitss.t1payment.model;

import java.util.Date;

public class SystemIdModel {
	
	
	private String id;
	private String systemId;
	private Integer module;
	private Date creationDate;
	private String hostName;
	private String requestQueue;
	private String responseQueue;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public Integer getModule() {
		return module;
	}
	public void setModule(Integer module) {
		this.module = module;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getRequestQueue() {
		return requestQueue;
	}
	public void setRequestQueue(String requestQueue) {
		this.requestQueue = requestQueue;
	}
	public String getResponseQueue() {
		return responseQueue;
	}
	public void setResponseQueue(String responseQueue) {
		this.responseQueue = responseQueue;
	}
	@Override
	public String toString() {
		return "SystemIdModel [id=" + id + ", systemId=" + systemId + ", module=" + module + ", creationDate="
				+ creationDate + ", hostName=" + hostName + ", requestQueue=" + requestQueue + ", responseQueue="
				+ responseQueue + "]";
	}
	

}
