package com.globalhitss.t1payment.controller;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.common.CommonUtils;
import com.globalhitss.t1payment.common.Messages;
import com.globalhitss.t1payment.common.TransLog;
import com.globalhitss.t1payment.entity.MerchantClaroPay;
import com.globalhitss.t1payment.entity.ServiceDetail;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.service.GatewayRequestBuilderService;
import com.globalhitss.t1payment.service.MerchantClaroPayService;
import com.globalhitss.t1payment.service.RemoteOperationFacade;
import com.globalhitss.t1payment.service.SubscriptionService;
import com.globalhitss.t1payment.util.AppWsUtil;
import com.globalhitss.t1payment.util.MessagesUtil;
import com.globalhitss.t1payment.util.TypeMessage;
import com.globalhitss.t1payment.vo.PayServiceTransaccion;
import com.globalhitss.t1payment.vo.request.T1CommercePaymentRequest;
import com.globalhitss.t1payment.vo.request.T1PaymentServiceRequest;
import com.globalhitss.t1payment.vo.response.GenericResponse;
import com.globalhitss.t1payment.vo.response.T1PaymentResponse;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * @author agonzalz
 */
@RestController
@RequestMapping("/v1/t1pagos")
public class T1PagosController extends BaseController {

    private static final Log LOG = LogFactory.getLog(T1PagosController.class);

    @Value("${code.ok}")
    private int codeOk;
    @Value("${code.pin.invalid}")
    private int codeInvalidPin;
    @Value("${code.internal.error}")
    private int codeInternalError;

    private final CommonUtils commonUtils;
    private final Messages messages;
    private final GatewayRequestBuilderService gatewayRequestBuilder;
    private final RemoteOperationFacade operationFacade;
    private final MerchantClaroPayService merchantClaroPayService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    public T1PagosController(CommonUtils commonUtils, Messages messages, GatewayRequestBuilderService gatewayRequestBuilder, RemoteOperationFacade operationFacade, MerchantClaroPayService merchantClaroPayService) {
        this.commonUtils = commonUtils;
        this.messages = messages;
        this.gatewayRequestBuilder = gatewayRequestBuilder;
        this.operationFacade = operationFacade;
        this.merchantClaroPayService = merchantClaroPayService;
    }

    @ApiOperation(value = "Commerce payment of T1Pagos", tags = "t1")
    @TransLog(operationName = "/commerce/charge")
    @PostMapping(value = "/commerce/charge")
    public ResponseEntity<T1PaymentResponse> t1Charge(@Valid @RequestBody T1CommercePaymentRequest request, BindingResult result) {
        /*validar que los datos cumplan las validaciones asignadas en el bean de T1CommercePaymentRequest*/
        T1PaymentResponse resp = new T1PaymentResponse();
        if (result.hasErrors()) {
            resp.setDataValidationErrors(getFieldErrors(result));
            resp.setResponseCode(999508);
            resp.setResponseMessage(messages.get("code.999508"));
            resp.setResponseSubject("subject.1s");
            return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
        }
        long messageId = this.commonUtils.getMessageId();
        resp.setTransId(LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli() + "");
        resp.setTimeStamp(ZonedDateTime.now().toInstant().toEpochMilli());

        try {
            resp.setExternalId(request.getExternalId());
            resp.setTransId(Long.toString(messageId));
            resp.setResponseCode(validateUser(request.getMsisdn()));
            if (resp.getResponseCode() != this.codeOk) {
                resp.setResponseMessage(messages.get(Messages.PREF_CODE + resp.getResponseCode()));
                resp.setResponseSubject(messages.get("subject.1s"));
                resp.setDataValidationErrors(Collections.emptyList());
                return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            // Obtencion de 'clabe account' a partir del 'merchantId'
            MerchantClaroPay merchantClaroPay = this.merchantClaroPayService.fingByMerchantId(request.getMerchantId());
            if (merchantClaroPay == null) {
                resp.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                resp.setResponseCode(this.codeInvalidPin);
                resp.setResponseMessage("Merchant id not found");
                resp.setResponseSubject(messages.get("subject.1s"));
                return new ResponseEntity<>(resp, HttpStatus.UNAUTHORIZED);
            }

            GatewayRequestMessage t1PaymentRequest = this.gatewayRequestBuilder.buildPaymentMicroappWithT1(messageId, request, merchantClaroPay.getBankClabe());
            GatewayResponseMessage context = this.operationFacade.RemoteTransferOperation(t1PaymentRequest, TypeMessage.T1_PAYMENT.getType());

            resp.setTimeStamp(t1PaymentRequest.getReceivedDate().getTime());
            resp.setTransId(String.valueOf(messageId));
            resp.setResponseSubject(context.getSubject());
            resp.setMessageType(Integer.valueOf(context.getMessageType()));
            resp.setDataValidationErrors(Collections.emptyList());
            return new ResponseEntity<>(resp, HttpStatus.OK);
        } catch (AppWsException e) {
            resp.setTimeStamp(Calendar.getInstance().getTimeInMillis());
            LOG.error("Error in transfer operation", e);
            resp.setResponseCode(e.getCode());
            resp.setResponseMessage(messages.get(resp.getResponseCode() + ""));
            resp.setResponseSubject(messages.get("subject.1s"));
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (NumberFormatException e) {
            LOG.error("Error in transfer operation", e);
            resp.setResponseCode(this.codeInternalError);
            resp.setResponseMessage(messages.get(Messages.PREF_CODE + this.codeInternalError));
            resp.setResponseSubject(messages.get("subject.1s"));
            return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @TransLog(operationName = "/v1/t1pagos/paymentServices")
    @PostMapping("/paymentServices")
    public ResponseEntity<GenericResponse> paymentServiceT1(@RequestBody T1PaymentServiceRequest request) {

        Date startDate = Calendar.getInstance().getTime();
        HttpStatus status = HttpStatus.PROCESSING;

        long transId = commonUtils.getMessageId();
        ThreadContext.push(String.valueOf("Transaccion Id: " + transId));

        LOG.info("Inicia Pago de Servicio /payments/serviceT1,  transId: " + transId);

        GenericResponse response = new GenericResponse();
        response.setTransId(String.valueOf(transId));
        response.setExternalId(request.getExternalId());

        try {
            LOG.info("Objecto T1PaymentServiceRequest-1:" + request.toString());

            Object[] mandatoryData = new Object[]{
                    request.getMsisdn(),
                    request.getExternalId(),
                    request.getAmount(),
                    request.getTransactionType(),
                    //Campo que guarda la referencia del pago
                    request.getDestAccountId()
            };

            // Validacion de campos obligatorios
            if (!validateMandatoryData(mandatoryData)) {
                LOG.error("Faltan datos obligatorios en la solicitud");
                throw new AppWsException(messages.get("code.999508"),
                        Integer.parseInt(messages.get("code.parameters.error")),
                        messages.get("response.invalid.input"));
            }

            // Pendiente de Revisar contra quien se va a validar el usuario
            response.setResponseCode(validateUser(request.getMsisdn()));

            if (response.getResponseCode() != this.codeOk) {
                response.setResponseMessage(messages.get(Messages.PREF_CODE + response.getResponseCode()));
                response.setResponseSubject(messages.get("subject.1s"));
                return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            // Recupera informacion del servicio en base de datos
            ServiceDetail serviceDetail = subscriptionService.getServiceDetailByCode(request.getTransactionType());

            if (serviceDetail != null) {

                LOG.info("Objeto serviceDetail: " + serviceDetail.toString());

                Integer referenceId = 0;
                if (serviceDetail.getReference() != null && serviceDetail.getReference().getType() != null) {
                    referenceId = serviceDetail.getReference().getType();
                }

                if (referenceId == 9 && request.getDestAccountId() == null) {
                    request.setDestAccountId(request.getMsisdn());
                }

                LOG.info("Objecto T1PaymentServiceRequest-2: " + request.toString());

                GatewayRequestMessage requestGateway = gatewayRequestBuilder.buildPaymentServiceT1RequestMessage(
                        transId, request,
                        serviceDetail.getReference() != null ? serviceDetail.getReference().getCode() : null);

                LOG.info("Objecto GatewayRequestMessage: " + requestGateway.toString());

                GatewayResponseMessage responseGateway = operationFacade.RemoteTransferOperation(requestGateway,
                        TypeMessage.T1_PAYMENT.getType());

                Calendar cal = Calendar.getInstance();
                cal.setTime(requestGateway.getReceivedDate());
                response.setTimeStamp(cal.getTimeInMillis());
                response.setTransId(requestGateway.getMessageId());

                LOG.info("Objecto GatewayResponseMessage: " + responseGateway.toString());
                LOG.info("Objecto GenericResponse: " + response.toString());

                MessagesUtil.setResponseMessage(response, responseGateway, this.messages);

                response.setResponseCode(response.getResponseCode() == 0 ? this.codeOk : response.getResponseCode());
                response.setResponseSubject(this.messages.get("subject.0s"));
                response.setMessageType(Integer.valueOf(responseGateway.getMessageType()));

                if (response.getResponseCode() == 0) {

                    PayServiceTransaccion logTransPayService = new PayServiceTransaccion();
                    logTransPayService.setInicioTransaccion(startDate);
                    logTransPayService.setFinTransaccion(Calendar.getInstance().getTime());
                    logTransPayService.setMensajeId(transId);
                    logTransPayService.setEnvia(request.getMsisdn());
                    logTransPayService.setRecibe(request.getDestAccountId());
                    logTransPayService.setMonto(String.valueOf(request.getAmount()));
                    logTransPayService.setCanal("AppWS");
                    logTransPayService.setCodigoRespuesta(0);
                    logTransPayService.setDescripcionRespuesta("");
                    logTransPayService.setExternalId(response.getExternalId());
                    logTransPayService.setLongitude(request.getLongitude());
                    logTransPayService.setLatitude(request.getLatitude());
                    AppWsUtil.toPaymentLog(logTransPayService);

                } else if (responseGateway.getCode() == 1169) {
                    response.setResponseCode(responseGateway.getCode());
                    response.setResponseMessage(responseGateway.getMessage());
                    response.setResponseSubject(responseGateway.getSubject());

                }

            } else {
                // No se encuentra el servicio en Base de Datos
                response.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                response.setResponseCode(999103);
                response.setResponseMessage(messages.get("code.999103"));
                response.setResponseSubject(messages.get("subject.1s"));
                return new ResponseEntity<GenericResponse>(response, HttpStatus.NOT_FOUND);

            }

            status = HttpStatus.OK;

        } catch (AppWsException e) {
            LOG.error("AppWsException ---> CodigoError: " + e.getCode() + ", MsgError: " + e.getMessage(), e);

            if (e.getCode() == Integer.parseInt(messages.get("code.parameters.error"))) {
                response.setTimeStamp(Calendar.getInstance().getTimeInMillis());
                response.setResponseCode(Integer.parseInt(messages.get("code.parameters.error")));
                response.setResponseMessage(messages.get("response.invalid.input"));
                response.setResponseSubject(messages.get("subject.1s"));
                status = HttpStatus.BAD_REQUEST;
            }

        } catch (Exception e) {
            LOG.error("A error has ocurred: " + e.getMessage(), e);
            response.setResponseCode(Integer.parseInt(messages.get("code.internal.error")));
            response.setResponseMessage(messages.get("response.general.error"));
            response.setResponseSubject(messages.get("subject.1s"));
            status = HttpStatus.INTERNAL_SERVER_ERROR;

        } finally {
            ThreadContext.clearAll();

        }

        LOG.info("Termina pago de servicio /payments/serviceT1");

        return new ResponseEntity<GenericResponse>(response, status);

    }

}
