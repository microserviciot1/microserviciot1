package com.globalhitss.t1payment.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindingResult;

import com.globalhitss.t1payment.vo.request.GenericRequest;


public abstract class BaseController {

	private final static Log log = LogFactory.getLog(BaseController.class);

	public String getConsumerKey(GenericRequest request, String authorization) {
		if (request.getConsumerKey() == null && authorization != null) {
			String[] auth = authorization.split(",");
			String[] auth2 = auth[0].split("OAuth oauth_consumer_key");
			String consumerKey = auth2[1].replaceAll("\"", "");
			return consumerKey;
		} else {
			return request.getConsumerKey();
		}
	}



	public boolean validateMandatoryData(Object[] mandatoryData) {
		for (Object data : mandatoryData) {
			if (data == null) {
				log.info("missing parameter mandatory");
				return false;
			}
		}
		return true;
	}

	/* MOCK */
	public Integer validateUser(String msisdn) {

		return 0;
	}
	
	protected List<String> getFieldErrors(BindingResult result) {
        return result.getFieldErrors().stream()
                .map(error -> "El campo '" + error.getField() + "' " + error.getDefaultMessage())
                .collect(Collectors.toList());
    }
	
}
