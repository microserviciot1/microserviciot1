package com.globalhitss.t1payment.util;

public enum TypeMessage {
	ACCOUNT_INFO(1),
	APPLY_FOR_DEBIT_CARD(2),
	CALL_ME(3),
	CASH_OUT(4),
	CHANGE_PIN(5),
	CHANGE_SERVICE_STATUS_TO_LOCK(6),
	CHANGE_SERVICE_STATUS_TO_UNLOCK(7),
	CHARGE(8),
	CUSTOMER_DATA(9),
	FUND_ACCOUNT(10),
	GET_BALANCE(11),
	GET_OTP(12),
	NEW_PIN(13),
	RECHARGE(14),
	REGISTER_CUSTOMER(15),
	TOKEN_SMS(16),
	TRANSACTION_HISTORY(17),
	TRANSFER_TO_REGULAR(18),
	TRANSFER_TO_SVA(19),
	VALIDATE_PIN(20),
	VALIDATE_MSISDN_ARRAY(21),
	PENDING_AMOUNT(22),
	PAYMENT_SERVICES(23),
	POCKET_LIMITS(24),
	UPDATE_BANKS(25),
	EDIT_CUSTOMER_DATA(26),
	LIMITS(27),
	SEND_CLARIFICATION(28),
	SET_PIN(29),
	
	VALIDATE_REGISTRATION(33),
	T1_PAYMENT(34),
	PAYMENT_MERCHANT(35)
	
	;
	private int type;
	
	private TypeMessage(int type) {
		this.type = type;
	}
	
	public int getType() {
		return type;
	}
	
    public static String getEnumByInt(int type){
        for(TypeMessage e : TypeMessage.values()){
            if(type == e.type) return e.name();
        }
        return null;
    }
}
