package com.globalhitss.t1payment.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class SystemUtil {

	private static int txIdIncrmentator = 0;
	private static final String DATE_FORMAT_CURRENT_DATE = "dd-MM-yyyy";
	private static final String DATE_FORMAT_CURRENT_DATE_SMS = "dd-MM-yy H:mm";
	private static final String DATE_FORMAT_CORRELATION_ID = "yyMMddHHmm";
	private static final String DECIMAL_FORMAT_CORRELATION_ID = "0000";
	private static final String DATE_FORMAT_TRANS_LOG = "HH:mm:ss:SSS";

	public static String getCurrentDateTime() {
		DateFormat formater = new SimpleDateFormat(DATE_FORMAT_CURRENT_DATE);
		return formater.format(new Date());
	}

	public static String getCurrentDateSMSFormat() {
		DateFormat formater = new SimpleDateFormat(DATE_FORMAT_CURRENT_DATE_SMS);
		return formater.format(new Date());
	}

	public static String getCurrentDateTransLog(Date date) {
		DateFormat formaterTransLog = new SimpleDateFormat(DATE_FORMAT_TRANS_LOG);
		return formaterTransLog.format(date);
	}

	public static String getCurrentDateStringTransLog(Date date) {
		DateFormat formaterTransLog = new SimpleDateFormat("yyyy-MM-dd");
		return formaterTransLog.format(date);
	}

	public static long getCorrelationId(short systemId) {
		int localIndex;
		synchronized (SystemUtil.class) {
			if (txIdIncrmentator >= 9999) {
				txIdIncrmentator = 0;
			}
			localIndex = ++txIdIncrmentator;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm");
		Date date = new Date();
		StringBuilder sb = new StringBuilder();
		sb.append(systemId);
		sb.append(formatter.format(date));
		DecimalFormat df = new DecimalFormat("0000");
		sb.append(df.format(localIndex));
		return Long.parseLong(sb.toString());
	}

	public static String getPositiveValue(String value) throws Exception {
		return Double.toString(Math.abs(Double.parseDouble(value)));
	}

	public static List<String> getStringListFromString(String commaSeperatedValues) {
		List<String> valueList = new ArrayList<>();
		if ((commaSeperatedValues != null) && (!commaSeperatedValues.trim().equals(""))) {
			StringTokenizer stringTokenizer = new StringTokenizer(commaSeperatedValues, ",");
			String value = "";
			while (stringTokenizer.hasMoreTokens()) {
				value = stringTokenizer.nextToken().trim();
				if (value != null) {
					valueList.add(value);
				}
			}
		}
		return valueList;
	}

	public static String formatMoney(double value) {
		DecimalFormat formatter = new DecimalFormat("###,##0.00");
		return formatter.format(value);
	}
	
}
