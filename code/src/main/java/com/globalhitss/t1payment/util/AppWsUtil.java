package com.globalhitss.t1payment.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globalhitss.t1payment.vo.PayServiceTransaccion;

public class AppWsUtil {

	private static Log logTrans = LogFactory.getLog("trans");
	private static Log logPayment = LogFactory.getLog("payment");
	private static Log log = LogFactory.getLog(AppWsUtil.class);
	
	public static final int INSERT = 1;
	public static final int UPDATE = 2;
	
	public static final int ALPHANUMERIC_OTP = 0;
	public static final int NUMERIC_OTP = 1;
	public static final int ALPHABETIC_OTP = 2;
	
	public static final int NOT_REQUIRED = 0;
	public static final int REQUIRED = 1;
	public static final int OPTIONAL = 2;

	public static final String ENROLL_ACTIVE_USER = "ACTIVE";
	public static final int ENROLL_OTB_OCRA = 0;
	public static final int ENROLL_OTB_NORMAL = 1;
	public static final int ENROLL_EVENT_OATH = 2;
	public static final int ENROLL_DYNAMIC_CVV_OTB = 3;
	public static final int ENROLL_DUAL_SEED = 4;
	public static final int ENROLL_BANAMEX_CVV=5;
	public static final int ENROLL_INBURSA_CVV=6;
	
	public static final int TRANS_KEYWORD=2;
	
	public static final String TYPE_PRIVACY_POLICY = "P";
	public static final String TYPE_TERMS_AND_CONDITIONS = "T";
	public static final String TYPE_TC_AND_PP = "A";

	private static long getTimeDifference(Date startTime, Date endTime) {
		log.debug("Get the time difference");
		if ((startTime != null) && (endTime != null))
			return (endTime.getTime() - startTime.getTime());
		return -1L;
	}

	public static void toPaymentLog(PayServiceTransaccion payServicio) {
		log.debug("Log into the Payment log file");
		try {
			DecimalFormat df = new DecimalFormat("0.00");
			payServicio.setMonto( (payServicio.getMonto() == null || payServicio.getMonto().isEmpty()) ? "0" : payServicio.getMonto().replaceAll(",", ""));
			payServicio.setMonto(  df.format(new BigDecimal(payServicio.getMonto()))  );
		} catch (Exception ex) {
			log.error("Error with decimal format", ex);
		}
		
		// Payment Reccord:
		// date|startTime|endTime|diff|messageId|externalId|msisdn|receiver|pendingAmountId|
		// amount|channel|status|errorCode|errorDesc|longitude|latitude
		
		StringBuilder str = new StringBuilder()
				.append((payServicio.getInicioTransaccion() == null) ? "" : SystemUtil.getCurrentDateStringTransLog(payServicio.getInicioTransaccion())).append("|")
				.append((payServicio.getInicioTransaccion() == null) ? "" : SystemUtil.getCurrentDateTransLog(payServicio.getInicioTransaccion())).append("|")
				.append((payServicio.getFinTransaccion() == null) ? "" : SystemUtil.getCurrentDateTransLog(payServicio.getFinTransaccion())).append("|")
				.append(getTimeDifference(payServicio.getInicioTransaccion(), payServicio.getFinTransaccion())).append("|").append(payServicio.getMensajeId()).append("|")
				.append((payServicio.getExternalId() == null) ? "" : payServicio.getExternalId()).append("|").append((payServicio.getEnvia() == null) ? "" : payServicio.getEnvia())
				.append("|").append((payServicio.getRecibe() == null) ? "" : payServicio.getRecibe()).append("|")
				.append(payServicio.getMonto()).append("|")
				.append((payServicio.getCanal() == null) ? "" : payServicio.getCanal()).append("|").append(( payServicio.getCodigoRespuesta() == 0) ? "SUCCESS" : "FAILED")
				.append("|").append(payServicio.getCodigoRespuesta()).append("|").append((payServicio.getDescripcionRespuesta() == null) ? "" : payServicio.getDescripcionRespuesta()).append("|")
				.append((payServicio.getLongitude() == null) ? "" : payServicio.getLongitude()).append("|").append((payServicio.getLatitude() == null) ? "" : payServicio.getLatitude());
		logPayment.info(str.toString());
		
	}

}
