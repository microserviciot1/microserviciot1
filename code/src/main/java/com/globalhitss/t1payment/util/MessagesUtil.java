package com.globalhitss.t1payment.util;

import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.common.Messages;
import com.globalhitss.t1payment.vo.response.GenericResponse;


/**
 *
 * @author agonzalz
 */
public class MessagesUtil {

	public static GenericResponse setResponseMessageOnlyContextMsg(GenericResponse resp, GatewayResponseMessage context,
			Messages messages) {
		if (context.isSuccess() && resp.getResponseCode() == 0) {
			resp.setResponseCode(context.getCode());
			resp.setResponseMessage(context.getMessage());
			resp.setResponseSubject(context.getSubject());
			resp.setMessageType(context.getMessageType() == null ? 0 : Integer.valueOf(context.getMessageType()));
		} else if (resp.getResponseCode() == 0) {
			resp.setResponseCode(-1);
			resp.setResponseMessage(messages.get(Messages.PREF_CODE + "-1"));
			resp.setResponseSubject(messages.get("subject.1s"));
		} else {
			resp.setResponseMessage(messages.get(resp.getResponseCode() + ""));
			resp.setResponseSubject(messages.get("subject.1s"));
		}
		return resp;
	}

	public static GenericResponse setResponseMessage(GenericResponse resp, GatewayResponseMessage context,
			Messages messages) {
		if (context.isSuccess() && resp.getResponseCode() == 0 && context.getCode() == 0) {
			resp.setResponseCode(context.getCode());
			resp.setResponseMessage(context.getMessage());
			resp.setResponseSubject(context.getSubject());
		} else if (resp.getResponseCode() == 0) {
			if (context.isReceived()) {
				resp.setResponseCode(context.getCode() == 0 ? 999400 : context.getCode());
				resp.setResponseCode(context.getCode() == 401 ? 999401 : context.getCode());
				resp.setResponseSubject(context.getSubject());
				if (null == messages.get(String.valueOf(resp.getResponseCode()))) {
					resp.setResponseMessage(messages.get(String.valueOf(resp.getResponseCode())));
				} else {
					resp.setResponseMessage(context.getMessage() != null ? context.getMessage()
							: messages.get(String.valueOf(resp.getResponseCode())));
				}
			} else {
				resp.setResponseCode(-1);
				resp.setResponseMessage(messages.get(String.valueOf("-1")));
				resp.setResponseSubject(messages.get("subject.1s"));
			}
		} else {
			resp.setResponseMessage(messages.get(String.valueOf(resp.getResponseCode())));
			resp.setResponseSubject(messages.get("subject.1s"));
		}
		return resp;
	}

}
