package com.globalhitss.t1payment.util;

public class Constants {
	/**
	 * identificador de componente, se toma del catalogo de la base de datos 
	 * */
	public static final Integer T1_PAYMENT_MODULE = 2;
	
	/**
	 * Prefijo de la cola sobre la que se este componente envia los mensajes
	 * */
	public static final String PREFIX_QUEUE_REQUEST = "claro.payment.t1.queue.request";
	public static final String PREFIX_QUEUE_RESPONSE = "claro.payment.t1.queue.response";
	public static final String DEFAULT_CHANNEL_ROUTING = "T";
	public static final String DECIMAL_FORMAT = "00.0";
}
