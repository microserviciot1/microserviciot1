package com.globalhitss.t1payment.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalhitss.t1payment.entity.MerchantClaroPay;
import com.globalhitss.t1payment.repository.MerchantClaroPayRepository;
import com.globalhitss.t1payment.service.MerchantClaroPayService;

/**
 *
 * @author agonzalz
 */
@Service
public class MerchantClaroPayServiceImpl implements MerchantClaroPayService {

	private final MerchantClaroPayRepository merchantClaroPayRepository;

	@Autowired
	public MerchantClaroPayServiceImpl(MerchantClaroPayRepository merchantClaroPayRepository) {
		this.merchantClaroPayRepository = merchantClaroPayRepository;
	}

	@Override
	public MerchantClaroPay fingByMerchantId(String merchantId) {
		return this.merchantClaroPayRepository.findByMerchantId(merchantId);
	}

}
