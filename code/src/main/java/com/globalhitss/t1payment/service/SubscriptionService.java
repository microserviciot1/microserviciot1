package com.globalhitss.t1payment.service;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.entity.AppPendingAmount;
import com.globalhitss.t1payment.entity.ServiceDetail;
import com.globalhitss.t1payment.vo.request.PaymentServiceRequest;


public interface SubscriptionService {
	
	ServiceDetail getServiceDetailByCode(String code);
	
	GatewayRequestMessage getRequestMessage(PaymentServiceRequest request, Long messageId);
	GatewayResponseMessage payment(GatewayRequestMessage requestMessage);
	
	AppPendingAmount getPendingAmountById(Long pendingAmountId);
	
	void updatePendingAmount(AppPendingAmount pendingAmount);
	
}
