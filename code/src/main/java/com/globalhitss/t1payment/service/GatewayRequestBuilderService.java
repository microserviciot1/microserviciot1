package com.globalhitss.t1payment.service;


import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.vo.request.T1CommercePaymentRequest;
import com.globalhitss.t1payment.vo.request.T1PaymentServiceRequest;

public interface GatewayRequestBuilderService {

	GatewayRequestMessage buildPaymentMicroappWithT1(Long messageId, T1CommercePaymentRequest request, String merchantBankClabe) throws AppWsException;
	
	GatewayRequestMessage buildPaymentServiceT1RequestMessage(Long messageId, T1PaymentServiceRequest request, String destAccountIdType) throws AppWsException;
	
}
