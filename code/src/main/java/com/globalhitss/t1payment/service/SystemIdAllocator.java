package com.globalhitss.t1payment.service;

import com.globalhitss.t1payment.entity.SystemIdEntity;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.model.SystemIdModel;

public interface SystemIdAllocator {
	
	SystemIdEntity create(SystemIdEntity entity);
	SystemIdModel getSystemIdByHostNameAndModule(String hostName, Integer module) throws AppWsException;
}
