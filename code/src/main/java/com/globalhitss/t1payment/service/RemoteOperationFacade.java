package com.globalhitss.t1payment.service;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.exception.AppWsException;


public interface RemoteOperationFacade {
	
	GatewayResponseMessage RemoteTransferOperation(GatewayRequestMessage requestMessage, int typeMessage) throws AppWsException;

}
