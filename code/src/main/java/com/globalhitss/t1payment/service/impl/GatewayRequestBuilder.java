package com.globalhitss.t1payment.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.globalhitss.integrationws.enumerator.AccessMethod;
import com.globalhitss.integrationws.enumerator.AccountNumberType;
import com.globalhitss.integrationws.enumerator.CountryCode;
import com.globalhitss.integrationws.enumerator.OperationType;
import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.request.T1PaymentClaroPay;
import com.globalhitss.integrationws.payload.request.T1PaymentServiceClaroPay;
import com.globalhitss.t1payment.enumerator.TenDigitsCode;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.model.SystemIdModel;
import com.globalhitss.t1payment.service.GatewayRequestBuilderService;
import com.globalhitss.t1payment.util.Constants;
import com.globalhitss.t1payment.vo.request.T1CommercePaymentRequest;
import com.globalhitss.t1payment.vo.request.T1PaymentServiceRequest;


@Component("gatewayRequestBuilder")
public class GatewayRequestBuilder implements GatewayRequestBuilderService {
	
	private static final Log LOG = LogFactory.getLog(GatewayRequestBuilder.class);
	
	@Autowired
	private SystemIdModel catSystemId;
	
	@Value("${lada.Mex}")
	private String ladaMex;
	
	

	@Override
	public GatewayRequestMessage buildPaymentMicroappWithT1(Long messageId, T1CommercePaymentRequest request, String merchantBankClabe) throws AppWsException {
		GatewayRequestMessage requestMsg = new GatewayRequestMessage(OperationType.PAYMENT_MICROAPP_WITH_T1.getValue());
		requestMsg.setConsumerKey(request.getConsumerKey());
		requestMsg.setExternalId(request.getExternalId());
		requestMsg.setMessageId(String.valueOf(messageId));
		requestMsg.setReceivedDate(new Date());

		T1PaymentClaroPay payload = new T1PaymentClaroPay();
		/* Generic operation attrs */
		payload.setRequestQueue(this.catSystemId.getRequestQueue());
		payload.setResponseQueue(this.catSystemId.getResponseQueue());
		payload.setSystemDate(new Date());
		payload.setOriginatorTransactionId(String.valueOf(messageId));
		payload.setCountryCode(CountryCode.MEXICO.getValue());
		payload.setAccessMethod(AccessMethod.SMARTPHONE_APP.getValue());
		payload.setMsisdn(request.getMsisdn().length() == 12 ? request.getMsisdn() : ladaMex + request.getMsisdn());
		payload.setLongitude(request.getLongitude());
		payload.setLatitude(request.getLatitude());
		/* Specific operation attrs */
		try {
			BigDecimal amount = new BigDecimal(request.getAmount().replaceAll(",", ""));
			DecimalFormat df = new DecimalFormat(Constants.DECIMAL_FORMAT);
			payload.setAmount(df.format(amount));
		} catch (Exception ex) {
			LOG.error("Error with decimal format", ex);
			throw new AppWsException("Error en datos enviados", 999508, "Error with decimal format");
		}
		payload.setReference(request.getAlphanumericReference());
		payload.setCardToken(request.getCardToken());
		payload.setMerchantId(request.getMerchantId());
		payload.setClaroUserId(request.getClaroUserId());
		payload.setDescription(request.getDescription());
		payload.setClabeAccountCommerce(merchantBankClabe);
		payload.setDeviceFingerPrint(request.getFingerPrint());
		payload.setClientId(request.getClientId());
		payload.setChannelRouting(Constants.DEFAULT_CHANNEL_ROUTING);
		requestMsg.setPayload(payload);
		return requestMsg;
	}
	
	@Override
	public GatewayRequestMessage buildPaymentServiceT1RequestMessage(Long transId, T1PaymentServiceRequest request, String destAccountIdType) throws AppWsException {

		GatewayRequestMessage requestMessage = new GatewayRequestMessage(OperationType.PAYMENT_SERVICE_WITH_T1.getValue());
		requestMessage.setConsumerKey(request.getConsumerKey());
		requestMessage.setExternalId(request.getExternalId());
		requestMessage.setMessageId(String.valueOf(transId));
		requestMessage.setReceivedDate(new Date());

		T1PaymentServiceClaroPay payload = new T1PaymentServiceClaroPay();

		payload.setSystemDate(new Date());
		payload.setRequestQueue(this.catSystemId.getRequestQueue());
		payload.setResponseQueue(this.catSystemId.getResponseQueue());

		LOG.info("Queue Request: " + payload.getRequestQueue());
		LOG.info("Queue Response: " + payload.getResponseQueue());

		payload.setOriginatorTransactionId(String.valueOf(transId));
		payload.setCountryCode(CountryCode.MEXICO.getValue());
		payload.setCurrency(CountryCode.MEXICO);
		payload.setAccessMethod(AccessMethod.SMARTPHONE_APP.getValue());
		payload.setMsisdn(request.getMsisdn().length() == 12 ? request.getMsisdn() : ladaMex + request.getMsisdn());
		payload.setLongitude(request.getLongitude());
		payload.setLatitude(request.getLatitude());

		payload.setTransactionType(request.getTransactionType());
		payload.setSourceAccountId(payload.getMsisdn());
		payload.setSourceAccountIdType(AccountNumberType.MSISDN);

		// Se solicito crear un excepcion para AT&T(TransactionType 306), para mandar el MSISDN a 10 digitos.
		if (TenDigitsCode.existCode(request.getTransactionType())) {
			payload.setDestAccountId(request.getDestAccountId());
		} else {
			payload.setDestAccountId(
					AccountNumberType.MSISDN.getValue().equals(destAccountIdType) ? ladaMex + request.getDestAccountId()
							: request.getDestAccountId());
		}

		payload.setDestAccountIdType(destAccountIdType);
		//payload.setPin(AESEncrypt.encryptToString(request.getPin(), this.appwsNipKey));

		try {
			BigDecimal amount = new BigDecimal(String.valueOf(request.getAmount()).replaceAll(",", ""));
			DecimalFormat df = new DecimalFormat(Constants.DECIMAL_FORMAT);
			payload.setTransactionAmount(df.format(amount));
		} catch (Exception ex) {
			LOG.error("Error with decimal format", ex);
			throw new AppWsException("Error en datos enviados", 999508, "Error with decimal format");
		}

		// Informacion requerida para procesar pago con API T1 Pagos
		payload.setTransactionDescription(request.getPaymentDescription());
		payload.setCardToken(request.getCardToken());
		payload.setReference(request.getDestAccountId());
		payload.setClientIdT1(request.getClientIdT1());
		payload.setDeviceFingerPrint(request.getDeviceFingerPrint());
		payload.setProductKey(request.getProductKey());

		LOG.info("Objeto T1PaymentServiceClaroPay(Payload): " + payload.toString());

		requestMessage.setPayload(payload);

		return requestMessage;
	}


}