package com.globalhitss.t1payment.service.impl;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globalhitss.t1payment.entity.SystemIdEntity;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.model.SystemIdModel;
import com.globalhitss.t1payment.repository.SystemIdRepository;
import com.globalhitss.t1payment.service.SystemIdAllocator;
import com.globalhitss.t1payment.util.Constants;


@Service
public class SystemIdAllocatorImpl implements SystemIdAllocator {
	
	private static final Log LOG = LogFactory.getLog(SystemIdAllocatorImpl.class);
	
	@Autowired
	private SystemIdRepository systemIdRepository;
	
	@Override
	public SystemIdEntity create(SystemIdEntity entity) {
		
		return null;
	}

	@Override
	public SystemIdModel getSystemIdByHostNameAndModule(String hostName, Integer module) throws AppWsException{
		try {
			LOG.info("hostName: " +  hostName + " - module: " + module);
			SystemIdEntity entity = systemIdRepository.getByHostNameAndModule(hostName, module);
			if(entity != null) {
				LOG.info("SystemId found to this component: \n" + entity.toString()  );
				return CatalogSystemIdEntity2Model(entity);
			}else {
				LOG.info("SystemId not found, create a new in DB");
				Integer maxSystemId = systemIdRepository.maxSystemIdToModule(module);
				if(maxSystemId == null) maxSystemId = 0;
				Integer nextSystemId = ++maxSystemId;
				LOG.info("new systemId calculated: " + nextSystemId);
				entity = new SystemIdEntity();
				entity.setCreationDate(new Date());
				entity.setHostName(hostName);
				entity.setModule(Constants.T1_PAYMENT_MODULE);
				entity.setSystemId(nextSystemId);
				entity.setRequestQueue(Constants.PREFIX_QUEUE_REQUEST);
				entity.setResponseQueue(Constants.PREFIX_QUEUE_RESPONSE + "." + nextSystemId);
				entity = systemIdRepository.save(entity);
				LOG.info("systemId created!! " +  entity.getSystemId());
				return CatalogSystemIdEntity2Model(entity);
			}
		}catch(Exception e) {
			LOG.error("Error obtaining systemId: " +  e.getMessage(), e);
			
			throw new AppWsException("message", -1, "reason");
		}
	}
	
	
	private SystemIdModel CatalogSystemIdEntity2Model(SystemIdEntity entity) {
		SystemIdModel model = new SystemIdModel();
		model.setCreationDate(entity.getCreationDate());
		model.setHostName(entity.getHostName());
		model.setModule(entity.getModule());
		model.setSystemId(String.valueOf(entity.getSystemId()));
		model.setId(entity.getId());
		model.setRequestQueue(entity.getRequestQueue());
		model.setResponseQueue(entity.getResponseQueue());
		return model;
	}

}
