package com.globalhitss.t1payment.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.jms.JmsMessageService;
import com.globalhitss.t1payment.service.RemoteOperationFacade;
import com.globalhitss.t1payment.util.TypeMessage;

/** This is the interface for consuming third-party software components */
@Service("remoteOperation")
public class RemoteOperationFacadeImpl implements RemoteOperationFacade {

	private static final Log LOG = LogFactory.getLog(RemoteOperationFacadeImpl.class);

	@Autowired
	@Qualifier("jmsMessageService")
	private JmsMessageService jmsService;

	@Override
	public GatewayResponseMessage RemoteTransferOperation(GatewayRequestMessage requestMessage, int typeMessage) throws AppWsException {

		GatewayResponseMessage responseMessage = new GatewayResponseMessage(Long.valueOf(requestMessage.getMessageId()),
				requestMessage.getPayload().getMsisdn(), false, false, typeMessage);
		try {
			LOG.info("SENDING OBJECTO TO API GATEWAY: "+  requestMessage.toString());
			jmsService.sendMessageToGateway(requestMessage);
			jmsService.registerToWait(responseMessage);

			LOG.info("Waiting for sync Response");

			responseMessage = jmsService.waitTillResponse(requestMessage.getMessageId(), 10000L);
			if (responseMessage == null || responseMessage.getCode() == null) {
				throw new AppWsException("Error wating for Core response TimeOut", -1,
						"Error wating for Core response TimeOut");
			}
		} catch (Exception e) {
			LOG.error("A error has ocurred in send message to amq " + e.getMessage(), e);
		} finally {
			LOG.info("FINISH OPERATION: " + TypeMessage.getEnumByInt(typeMessage));
		}
		return responseMessage;
	}

}
