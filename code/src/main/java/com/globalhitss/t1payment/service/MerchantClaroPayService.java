package com.globalhitss.t1payment.service;

import com.globalhitss.t1payment.entity.MerchantClaroPay;

/**
 *
 * @author agonzalz
 */
public interface MerchantClaroPayService {

	MerchantClaroPay fingByMerchantId(String merchantId);

}
