package com.globalhitss.t1payment.service.impl;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.globalhitss.integrationws.enumerator.AccessMethod;
import com.globalhitss.integrationws.enumerator.CountryCode;
import com.globalhitss.integrationws.enumerator.OperationType;
import com.globalhitss.integrationws.payload.request.GatewayRequestMessage;
import com.globalhitss.integrationws.payload.request.PaymentServiceClaroPay;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.entity.AppPendingAmount;
import com.globalhitss.t1payment.entity.ServiceDetail;
import com.globalhitss.t1payment.jms.JmsMessageService;
import com.globalhitss.t1payment.repository.AppPendingAmountRepository;
import com.globalhitss.t1payment.repository.ServiceDetailRepository;
import com.globalhitss.t1payment.service.SubscriptionService;
import com.globalhitss.t1payment.vo.request.PaymentServiceRequest;


@Service("subscriptionService")
public class SubscriptionServiceImpl implements SubscriptionService {
	
	private static final Log LOG = LogFactory.getLog(SubscriptionServiceImpl.class);
	
	@Autowired
	private ServiceDetailRepository serviceDetailRepository;
	
	@Autowired
	private AppPendingAmountRepository appPendingAmountRepository;
	
	@Autowired
	@Qualifier("jmsMessageService")
	private JmsMessageService jmsMessageService;
	
	@Override
	public ServiceDetail getServiceDetailByCode(String code) {
		LOG.info("SEARCHING BY code: " +  code);
		return serviceDetailRepository.getServiceDetailByCode(code);
	}

	@Override
	public GatewayRequestMessage getRequestMessage(PaymentServiceRequest request, Long messageId) {
		GatewayRequestMessage requestMessage = new GatewayRequestMessage();
		
		requestMessage.setOperationType(OperationType.PAYMENT_SERVICES.getValue());
		requestMessage.setConsumerKey(request.getConsumerKey());
		requestMessage.setExternalId(request.getExternalId());
		requestMessage.setMessageId(String.valueOf(messageId));
		requestMessage.setReceivedDate(new Date());
		
		PaymentServiceClaroPay payLoad = new PaymentServiceClaroPay(null);
		payLoad.setSystemDate(new Date());
		payLoad.setOriginatorTransactionId(String.valueOf(messageId));
		payLoad.setCountryCode(CountryCode.MEXICO.getValue());
		payLoad.setCurrency(CountryCode.MEXICO);
		payLoad.setAccessMethod(AccessMethod.SMARTPHONE_APP.getValue());
		
		requestMessage.setPayload(payLoad);
		
		return requestMessage;
	}
	
	/*
	 * 

	 * 
	 */

	@Override
	public GatewayResponseMessage payment(GatewayRequestMessage requestMessage) {
//		return jmsMessageService.waitTillResponse(requestMessage, null);
		return new GatewayResponseMessage();
	}

	@Override
	public AppPendingAmount getPendingAmountById(Long pendingAmountId) {
		try {
			return appPendingAmountRepository.getOne(pendingAmountId);
		}catch(Exception e) {
			LOG.error("A error has ocurred while getting pending amount in DB");
		}
		return null;
	}

	@Override
	public void updatePendingAmount(AppPendingAmount pendingAmount) {
		appPendingAmountRepository.save(pendingAmount);
		
	}

}
