package com.globalhitss.t1payment.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SERVICE_DETAIL")
public class ServiceDetail implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6234247256298118297L;

	@Id
	@Column(name = "SERVICE_ID", unique = true, length = 36)
	private String serviceId;

	@Column(name = "NAME", length = 250)
	private String name;

	@Column(name = "ALIAS_REF", length = 250)
	private String aliasReference;

	@Column(name = "ALIAS_NAME_PLACEHOLDER", length = 250)
	private String aliasNamePlaceHolder;

	@Column(name = "ALIAS_NAME", length = 250)
	private String aliasName;

	@Column(name = "REFERENCE_SCANNING_BTN")
	private Boolean referenceScanningBtn;

	@Column(name = "PAYMENT_MSG", length = 250)
	private String paymentMsg;

	@Column(name = "CODE", length = 4)
	private String code;

	@Column(name = "REFERENCE_SCANNING_MSG")
	private String referenceScanningMsg;

	@Column(name = "SMS_CODE", length = 30)
	private String smsCode;

	@Column(name = "TNC_MSG", length = 500)
	private String tncMsg;

	@Column(name = "BTN_MSG", length = 100)
	private String btnMsg;

	@Column(name = "SERVICE_LABEL", length = 500)
	private String serviceLabel;

	@Column(name = "SERVICE_LABEL2", length = 500)
	private String serviceLabel2;

	@Column(name = "QUERY_BALANCE")
	private Boolean queryBalance;

	@Column(name = "PRIORITY")
	private Integer priority;

	@Column(name = "PROMOTION")
	private Boolean promotion;

	@Column(name = "PROMOTION_AMOUNT")
	private Double amount;

	@Column(name = "PAYMENT_DUPLICITY")
	private Boolean paymentDuplicity;

	@Column(name = "BANK_ID")
	private String bankId;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "BANK_TO_SERVICE", joinColumns = @JoinColumn(name = "SERVICE_ID"),
	inverseJoinColumns = @JoinColumn(name="BANK_ID"))
	private Set<BankEntity> banks;
	
	@Column(name="ACCOUNT_NUMBER", length=60)
	private String accountNumber;
	
	@Column(name="ONLINE_ACCOUNT_NUMBER", length=60)
	private String onlineAccountNumber;
	
	@Column(name="STATUS")
	private Boolean status;
	
	@Column(name="URL_HELP_TITLE")
	private String urlHelpTitle;

	@Column(name="APPLY_AMOUNT")
	private boolean applyAmount;
	
	@Column(name="AMOUNT_LABEL", length=250)
	private String amountLabel;
	
	@Column(name="OPEN_AMOUNT")
	private Boolean openAmount;
	
	@Column(name="AMOUNT_TYPE")
	private Integer amountType;
	
	@Column(name="AMOUNT_LIST", length=2048)
	private String amountList;
	
	@Column(name="MIN_PAYMENT_AMOUNT")
	private Integer minPaymentAmount;
	
	@Column(name="MAX_PAYMENT_AMOUNT")
	private Integer maxPaymentAmount;
	
	@Column(name="APPLY_REFERENCE")
	private Boolean applyReference;
	
	@Column(name="REFERENCE_LABEL", length=250)
	private String referenceLabel;
	
	@ManyToOne
	@JoinColumn(name="REFERENCE_ID")
	private ServiceReferenceEntity reference;
	
	@Column(name="REFERENCE_SIZE")
	private Integer referenceSize;
	
	@Column(name="REFERENCE_SIZE_MAX")
	private Integer referenceSizeMax;
	
	@Column(name="REFERENCE_SIZE_MIN")
	private Integer referenceSizeMin;
	
	@Column(name="REFERENCE_PLACEHOLDER", length=250)
	private String referencePlaceHolder;
	
	@Column(name="STORE_REFERENCE")
	private Boolean storeReference;
	
	@Column(name="APPLY_ALIAS")
	private Boolean applyAlias;
	
	@Column(name="ALIAS_LABEL", length=250)
	private String aliasLabel;
	
	@Column(name="ALIAS_INSTRUCTIONS", length=250)
	private String aliasInstructions;
	
	@Column(name="ALIAS_TYPE", length=20)
	private String aliasType;
	
	@Column(name="URL_HELP")
	private String urlHelp;
	
	@Column(name="ALIAS_PLACEHOLDER", length=250)
	private String aliasPlaceHolder;
	
	@Column(name="ALIAS_REF_PLACEHOLDER", length=250)
	private String aliasRefPlaceHolder;
	
	@Column(name="APPLY_PRODUCT")
	private Boolean applyProduct;
	
	@Column(name="PRODUCT_LABEL", length=250)
	private String productLabel;
	
	@Column(name="PRODUCT_DIALOG_MSG", length=500)
	private String productDialogMsg;
	
	@Column(name="PRODUCT_DIALOG_MSG2", length=500)
	private String productDialogMsg2;
	
	@Column(name="PRODUCT_SMS", length=500)
	private String productSms;
	
	@Column(name="productSms2", length=500)
	private String productSms2;
	
	@Column(name="FEE")
	private Boolean fee;
	
	@Column(name="FEE_AMOUNT")
	private String feeAmount;
	
	@Column(name="EMBEDDED_AMOUNT_REF")
	private Boolean embeddedAmountInReference;
	
	@Column(name="REF_AMOUNT_INIT")
	private Long referenceAmountInitIndex;
	
	@Column(name="REF_AMOUNT_END")
	private Long referenceAmountEndIndex;

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAliasReference() {
		return aliasReference;
	}

	public void setAliasReference(String aliasReference) {
		this.aliasReference = aliasReference;
	}

	public String getAliasNamePlaceHolder() {
		return aliasNamePlaceHolder;
	}

	public void setAliasNamePlaceHolder(String aliasNamePlaceHolder) {
		this.aliasNamePlaceHolder = aliasNamePlaceHolder;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public Boolean getReferenceScanningBtn() {
		return referenceScanningBtn;
	}

	public void setReferenceScanningBtn(Boolean referenceScanningBtn) {
		this.referenceScanningBtn = referenceScanningBtn;
	}

	public String getPaymentMsg() {
		return paymentMsg;
	}

	public void setPaymentMsg(String paymentMsg) {
		this.paymentMsg = paymentMsg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReferenceScanningMsg() {
		return referenceScanningMsg;
	}

	public void setReferenceScanningMsg(String referenceScanningMsg) {
		this.referenceScanningMsg = referenceScanningMsg;
	}

	public String getSmsCode() {
		return smsCode;
	}

	public void setSmsCode(String smsCode) {
		this.smsCode = smsCode;
	}

	public String getTncMsg() {
		return tncMsg;
	}

	public void setTncMsg(String tncMsg) {
		this.tncMsg = tncMsg;
	}

	public String getBtnMsg() {
		return btnMsg;
	}

	public void setBtnMsg(String btnMsg) {
		this.btnMsg = btnMsg;
	}

	public String getServiceLabel() {
		return serviceLabel;
	}

	public void setServiceLabel(String serviceLabel) {
		this.serviceLabel = serviceLabel;
	}

	public String getServiceLabel2() {
		return serviceLabel2;
	}

	public void setServiceLabel2(String serviceLabel2) {
		this.serviceLabel2 = serviceLabel2;
	}

	public Boolean getQueryBalance() {
		return queryBalance;
	}

	public void setQueryBalance(Boolean queryBalance) {
		this.queryBalance = queryBalance;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Boolean getPromotion() {
		return promotion;
	}

	public void setPromotion(Boolean promotion) {
		this.promotion = promotion;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getPaymentDuplicity() {
		return paymentDuplicity;
	}

	public void setPaymentDuplicity(Boolean paymentDuplicity) {
		this.paymentDuplicity = paymentDuplicity;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public Set<BankEntity> getBanks() {
		return banks;
	}

	public void setBanks(Set<BankEntity> banks) {
		this.banks = banks;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getOnlineAccountNumber() {
		return onlineAccountNumber;
	}

	public void setOnlineAccountNumber(String onlineAccountNumber) {
		this.onlineAccountNumber = onlineAccountNumber;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getUrlHelpTitle() {
		return urlHelpTitle;
	}

	public void setUrlHelpTitle(String urlHelpTitle) {
		this.urlHelpTitle = urlHelpTitle;
	}

	public boolean isApplyAmount() {
		return applyAmount;
	}

	public void setApplyAmount(boolean applyAmount) {
		this.applyAmount = applyAmount;
	}

	public String getAmountLabel() {
		return amountLabel;
	}

	public void setAmountLabel(String amountLabel) {
		this.amountLabel = amountLabel;
	}

	public Boolean getOpenAmount() {
		return openAmount;
	}

	public void setOpenAmount(Boolean openAmount) {
		this.openAmount = openAmount;
	}

	public Integer getAmountType() {
		return amountType;
	}

	public void setAmountType(Integer amountType) {
		this.amountType = amountType;
	}

	public String getAmountList() {
		return amountList;
	}

	public void setAmountList(String amountList) {
		this.amountList = amountList;
	}

	public Integer getMinPaymentAmount() {
		return minPaymentAmount;
	}

	public void setMinPaymentAmount(Integer minPaymentAmount) {
		this.minPaymentAmount = minPaymentAmount;
	}

	public Integer getMaxPaymentAmount() {
		return maxPaymentAmount;
	}

	public void setMaxPaymentAmount(Integer maxPaymentAmount) {
		this.maxPaymentAmount = maxPaymentAmount;
	}

	public Boolean getApplyReference() {
		return applyReference;
	}

	public void setApplyReference(Boolean applyReference) {
		this.applyReference = applyReference;
	}

	public String getReferenceLabel() {
		return referenceLabel;
	}

	public void setReferenceLabel(String referenceLabel) {
		this.referenceLabel = referenceLabel;
	}

	public ServiceReferenceEntity getReference() {
		return reference;
	}

	public void setReference(ServiceReferenceEntity reference) {
		this.reference = reference;
	}

	public Integer getReferenceSize() {
		return referenceSize;
	}

	public void setReferenceSize(Integer referenceSize) {
		this.referenceSize = referenceSize;
	}

	public Integer getReferenceSizeMax() {
		return referenceSizeMax;
	}

	public void setReferenceSizeMax(Integer referenceSizeMax) {
		this.referenceSizeMax = referenceSizeMax;
	}

	public Integer getReferenceSizeMin() {
		return referenceSizeMin;
	}

	public void setReferenceSizeMin(Integer referenceSizeMin) {
		this.referenceSizeMin = referenceSizeMin;
	}

	public String getReferencePlaceHolder() {
		return referencePlaceHolder;
	}

	public void setReferencePlaceHolder(String referencePlaceHolder) {
		this.referencePlaceHolder = referencePlaceHolder;
	}

	public Boolean getStoreReference() {
		return storeReference;
	}

	public void setStoreReference(Boolean storeReference) {
		this.storeReference = storeReference;
	}

	public Boolean getApplyAlias() {
		return applyAlias;
	}

	public void setApplyAlias(Boolean applyAlias) {
		this.applyAlias = applyAlias;
	}

	public String getAliasLabel() {
		return aliasLabel;
	}

	public void setAliasLabel(String aliasLabel) {
		this.aliasLabel = aliasLabel;
	}

	public String getAliasInstructions() {
		return aliasInstructions;
	}

	public void setAliasInstructions(String aliasInstructions) {
		this.aliasInstructions = aliasInstructions;
	}

	public String getAliasType() {
		return aliasType;
	}

	public void setAliasType(String aliasType) {
		this.aliasType = aliasType;
	}

	public String getUrlHelp() {
		return urlHelp;
	}

	public void setUrlHelp(String urlHelp) {
		this.urlHelp = urlHelp;
	}

	public String getAliasPlaceHolder() {
		return aliasPlaceHolder;
	}

	public void setAliasPlaceHolder(String aliasPlaceHolder) {
		this.aliasPlaceHolder = aliasPlaceHolder;
	}

	public String getAliasRefPlaceHolder() {
		return aliasRefPlaceHolder;
	}

	public void setAliasRefPlaceHolder(String aliasRefPlaceHolder) {
		this.aliasRefPlaceHolder = aliasRefPlaceHolder;
	}

	public Boolean getApplyProduct() {
		return applyProduct;
	}

	public void setApplyProduct(Boolean applyProduct) {
		this.applyProduct = applyProduct;
	}

	public String getProductLabel() {
		return productLabel;
	}

	public void setProductLabel(String productLabel) {
		this.productLabel = productLabel;
	}

	public String getProductDialogMsg() {
		return productDialogMsg;
	}

	public void setProductDialogMsg(String productDialogMsg) {
		this.productDialogMsg = productDialogMsg;
	}

	public String getProductDialogMsg2() {
		return productDialogMsg2;
	}

	public void setProductDialogMsg2(String productDialogMsg2) {
		this.productDialogMsg2 = productDialogMsg2;
	}

	public String getProductSms() {
		return productSms;
	}

	public void setProductSms(String productSms) {
		this.productSms = productSms;
	}

	public String getProductSms2() {
		return productSms2;
	}

	public void setProductSms2(String productSms2) {
		this.productSms2 = productSms2;
	}

	public Boolean getFee() {
		return fee;
	}

	public void setFee(Boolean fee) {
		this.fee = fee;
	}

	public String getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Boolean getEmbeddedAmountInReference() {
		return embeddedAmountInReference;
	}

	public void setEmbeddedAmountInReference(Boolean embeddedAmountInReference) {
		this.embeddedAmountInReference = embeddedAmountInReference;
	}

	public Long getReferenceAmountInitIndex() {
		return referenceAmountInitIndex;
	}

	public void setReferenceAmountInitIndex(Long referenceAmountInitIndex) {
		this.referenceAmountInitIndex = referenceAmountInitIndex;
	}

	public Long getReferenceAmountEndIndex() {
		return referenceAmountEndIndex;
	}

	public void setReferenceAmountEndIndex(Long referenceAmountEndIndex) {
		this.referenceAmountEndIndex = referenceAmountEndIndex;
	}

	@Override
	public String toString() {
		return "ServiceDetail [serviceId=" + serviceId + ", name=" + name + ", aliasReference=" + aliasReference
				+ ", aliasNamePlaceHolder=" + aliasNamePlaceHolder + ", aliasName=" + aliasName
				+ ", referenceScanningBtn=" + referenceScanningBtn + ", paymentMsg=" + paymentMsg + ", code=" + code
				+ ", referenceScanningMsg=" + referenceScanningMsg + ", smsCode=" + smsCode + ", tncMsg=" + tncMsg
				+ ", btnMsg=" + btnMsg + ", serviceLabel=" + serviceLabel + ", serviceLabel2=" + serviceLabel2
				+ ", queryBalance=" + queryBalance + ", priority=" + priority + ", promotion=" + promotion + ", amount="
				+ amount + ", paymentDuplicity=" + paymentDuplicity + ", bankId=" + bankId + ", banks=" + banks
				+ ", accountNumber=" + accountNumber + ", onlineAccountNumber=" + onlineAccountNumber + ", status="
				+ status + ", urlHelpTitle=" + urlHelpTitle + ", applyAmount=" + applyAmount + ", amountLabel="
				+ amountLabel + ", openAmount=" + openAmount + ", amountType=" + amountType + ", amountList="
				+ amountList + ", minPaymentAmount=" + minPaymentAmount + ", maxPaymentAmount=" + maxPaymentAmount
				+ ", applyReference=" + applyReference + ", referenceLabel=" + referenceLabel + ", reference="
				+ reference + ", referenceSize=" + referenceSize + ", referenceSizeMax=" + referenceSizeMax
				+ ", referenceSizeMin=" + referenceSizeMin + ", referencePlaceHolder=" + referencePlaceHolder
				+ ", storeReference=" + storeReference + ", applyAlias=" + applyAlias + ", aliasLabel=" + aliasLabel
				+ ", aliasInstructions=" + aliasInstructions + ", aliasType=" + aliasType + ", urlHelp=" + urlHelp
				+ ", aliasPlaceHolder=" + aliasPlaceHolder + ", aliasRefPlaceHolder=" + aliasRefPlaceHolder
				+ ", applyProduct=" + applyProduct + ", productLabel=" + productLabel + ", productDialogMsg="
				+ productDialogMsg + ", productDialogMsg2=" + productDialogMsg2 + ", productSms=" + productSms
				+ ", productSms2=" + productSms2 + ", fee=" + fee + ", feeAmount=" + feeAmount
				+ ", embeddedAmountInReference=" + embeddedAmountInReference + ", referenceAmountInitIndex="
				+ referenceAmountInitIndex + ", referenceAmountEndIndex=" + referenceAmountEndIndex + "]";
	}
	
	
}
