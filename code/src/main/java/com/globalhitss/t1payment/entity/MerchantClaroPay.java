package com.globalhitss.t1payment.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MERCHANT_CLARO_PAY")
public class MerchantClaroPay implements Serializable {

    private static final long serialVersionUID = -469139603684810096L;

    @Id
    @Column(name = "MERCHANT_ID", nullable = false, length = 36)
    private String merchantId;

    @Column(name = "BANK_CLABE", length = 60)
    private String bankClabe;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "BANK_ID")
    private BankEntity bankId;
    @Column(name = "NAME", length = 60)
    private String name;
    @Column(name = "BUSINESS_NAME", length = 60)
    private String businessName;
    @Column(name = "CONTACT", length = 200)
    private String contact;
    @Column(name = "ADDRESS", length = 250)
    private String address;
    @Column(name = "STATUS")
    private Integer status;
    @Column(name = "MERCHANT_COMMISSION")
    private Integer merchantCommission;
    @Column(name = "COMMISSION_AMOUNT")
    private Double comissionAmount;
    @Column(name = "COMMERCIAL_NAME", length = 60)
    private String commercialName;
    @Column(name = "STREET_NUMBER", length = 6)
    private String streetNumber;
    @Column(name = "LEGAL_GUARDIAN_NAME", length = 60)
    private String legalGuardianName;
    @Column(name = "LEGAL_GUARDIAN_EMAIL", length = 80)
    private String legalGuardianEmail;
    @Column(name = "NEIGHBORHOOD", length = 50)
    private String neighborhood;
    @Column(name = "ZIP_CODE_ID", length = 50)
    private String zipCodeId;
    @Column(name = "CREATED_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime createdDate;

    public MerchantClaroPay() {
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getBankClabe() {
        return bankClabe;
    }

    public void setBankClabe(String bankClabe) {
        this.bankClabe = bankClabe;
    }

    public BankEntity getBankId() {
        return bankId;
    }

    public void setBankId(BankEntity bankId) {
        this.bankId = bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getMerchantComission() {
        return merchantCommission;
    }

    public void setMerchantComission(Integer merchantComission) {
        this.merchantCommission = merchantComission;
    }

    public Double getComissionAmount() {
        return comissionAmount;
    }

    public void setComissionAmount(Double comissionAmount) {
        this.comissionAmount = comissionAmount;
    }

    public String getCommercialName() {
        return commercialName;
    }

    public void setCommercialName(String commercialName) {
        this.commercialName = commercialName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getLegalGuardianName() {
        return legalGuardianName;
    }

    public void setLegalGuardianName(String legalGuardianName) {
        this.legalGuardianName = legalGuardianName;
    }

    public String getLegalGuardianEmail() {
        return legalGuardianEmail;
    }

    public void setLegalGuardianEmail(String legalGuardianEmail) {
        this.legalGuardianEmail = legalGuardianEmail;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipCodeId() {
        return zipCodeId;
    }

    public void setZipCodeId(String zipCodeId) {
        this.zipCodeId = zipCodeId;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "MerchantClaroPay{" + "merchantId=" + merchantId + ", bankClabe=" + bankClabe + ", bankId=" + bankId + ", name=" + name + ", businessName=" + businessName + ", contact=" + contact + ", address=" + address + ", status=" + status + ", merchantComission=" + merchantCommission + ", comissionAmount=" + comissionAmount + ", commercialName=" + commercialName + ", streetNumber=" + streetNumber + ", legalGuardianName=" + legalGuardianName + ", legalGuardianEmail=" + legalGuardianEmail + ", neighborhood=" + neighborhood + ", zipCodeId=" + zipCodeId + ", createdDate=" + createdDate + '}';
    }

}
