package com.globalhitss.t1payment.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SERVICE_REFERENCE")
public class ServiceReferenceEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3941703330283088082L;
	
	@Id
	@Column(name="REFERENCE_ID")
	private Integer referenceId;
	
	@Column(name="CODE", length=5)
	private String code;
	
	@Column(name="DESCRIPTION", length=250)
	private String description;
	
	@Column(name="TYPE", columnDefinition="0=AlphaNumeric, 1=Numeric, 2=Alphabetic")
	private Integer type;

	public Integer getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	
}
