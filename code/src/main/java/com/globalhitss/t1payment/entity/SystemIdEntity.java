package com.globalhitss.t1payment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CAT_SYSTEM_ID")
public class SystemIdEntity implements Serializable {

    public SystemIdEntity() {
        super();
    }

    public SystemIdEntity(String hostName, Date creationDate, Integer module) {
        super();
        this.hostName = hostName;
        this.creationDate = creationDate;
        this.module = module;
    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID")
    private String id;

    @Column(name = "SYSTEM_ID", unique = true)
    private Integer systemId;

    @Column(name = "HOSTNAME", unique = true)
    private String hostName;

    @Column(name = "CREATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "MODULE")
    private Integer module;

    @Column(name = "REQUEST_QUEUE")
    private String requestQueue;

    @Column(name = "RESPONSE_QUEUE")
    private String responseQueue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSystemId() {
        return systemId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getRequestQueue() {
        return requestQueue;
    }

    public void setRequestQueue(String requestQueue) {
        this.requestQueue = requestQueue;
    }

    public String getResponseQueue() {
        return responseQueue;
    }

    public void setResponseQueue(String responseQueue) {
        this.responseQueue = responseQueue;
    }

    @Override
    public String toString() {
        return "SystemIdEntity [id=" + id + ", systemId=" + systemId + ", hostName=" + hostName + ", creationDate="
                + creationDate + ", module=" + module + ", requestQueue=" + requestQueue + ", responseQueue="
                + responseQueue + "]";
    }

}
