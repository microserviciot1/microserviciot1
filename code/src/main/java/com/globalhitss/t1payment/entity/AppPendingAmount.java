package com.globalhitss.t1payment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="APP_PENDING_AMOUNT")
public class AppPendingAmount implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4970258522927859029L;
	
	@Id
	@Column(name="PENDING_AMOUNT_ID", unique=true)
	private Long pendingAmountId;
	
	@Column(name="SERVICE_CODE", length=4)
	private String serviceCode;
	
	@Column(name="MSISDN", length=12)
	private String msisdn;
	
	@Column(name="AMOUNT")
	private Double amount;
	
	@Column(name="EXPIRATION_DATE")
	@Temporal(TemporalType.DATE)
	private Date expirationDate;
	
	@Column(name="PROCESSING_STATUS")
	private Integer processingStatus;
	
	@Column(name="BANK_ID", length=36)
	private String bankId;
	
	@Column(name="CODE", length=5)
	private String code;
	
	@Column(name="CODE_MESSAGE", length=250)
	private String codeMessage;
	
	@Column(name="CREATION_DATE")
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	
	@Column(name="MODIFICATION_DATE")
	@Temporal(TemporalType.DATE)
	private Date modificationDate;
	
	@Column(name="REFERENCE", length=10)
	private String reference;
	
	public Long getPendingAmountId() {
		return pendingAmountId;
	}
	public void setPendingAmountId(Long pendingAmountId) {
		this.pendingAmountId = pendingAmountId;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Integer getProcessingStatus() {
		return processingStatus;
	}
	public void setProcessingStatus(Integer processingStatus) {
		this.processingStatus = processingStatus;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodeMessage() {
		return codeMessage;
	}
	public void setCodeMessage(String codeMessage) {
		this.codeMessage = codeMessage;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	@Override
	public String toString() {
		return "AppPendingAmount [pendingAmountId=" + pendingAmountId + ", serviceCode=" + serviceCode + ", msisdn="
				+ msisdn + ", amount=" + amount + ", expirationDate=" + expirationDate + ", processingStatus="
				+ processingStatus + ", bankId=" + bankId + ", code=" + code + ", codeMessage=" + codeMessage
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + ", reference="
				+ reference + "]";
	}
	
	
	
}
