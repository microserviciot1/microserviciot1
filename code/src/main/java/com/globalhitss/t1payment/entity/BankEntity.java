package com.globalhitss.t1payment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="BANK")
public class BankEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public BankEntity() {}
	
	@Id
	@Column(name="BANK_ID")
	@GeneratedValue(generator="UUID")
	@GenericGenerator(name="UUID", strategy="org.hibernate.id.UUIDGenerator")
	private String bankId;
	
	@Column(name="NAME", length=255)
	private String name;
	
	@Column(name="STATUS")
	private Boolean status;
	
	@Column(name="ADMIN")
	private Boolean admin;

	@Column(name="BUSINESS_NAME", length=60)
	private String businessName;
	
	@Column(name="INITIALS", length=8)
	private String initials;
	
	@Column(name="ISO_CODE", length=3)
	private String isoCode;
	
	@Column(name="BANK_CODE", length=3)
	private String bankCode;
	
	@Column(name="EMAIL_DOMAIN")
	private String emailDomains;
	
	@Column(name="URL_REGISTER")
	private String urlRegister;
	
	@Column(name="PACKAGE_MODE")
	private String packageMode;
	
	@Column(name="PACKAGE_ACCOUNT")
	private String packageAccount;
	
	@Column(name="URL")
	private String url;
	
	@Column(name="URL_TERMS")
	private String urlTerms;
	
	@Column(name="CALL_CENTER")
	private String callCenter;
	
	@Column(name="CALL_CENTER_TEXT")
	private String callCenterText;
	
	@Column(name="CALL_CENTER_LABEL")
	private String callCenterLabel;
	
	@Column(name="BANK_CODE_REGISTER")
	private String bankCodeRegister;
	
	@Column(name="WORDING_SMS_TRANSFER")
	private String wordingSmsTransfer;
	
	@Column(name="MODIFICATION_DATE")
	private Date modificationDate;
	
	@Column(name="OP_TRANSFER")
	private Boolean opTransfer;
	
	@Column(name="OP_CASH")
	private Boolean opCash;
	
	@Column(name="OP_FUNDING")
	private Boolean opFounding;
	
	@Column(name="OP_CHARGE")
	private Boolean opCharge;
	
	@Column(name="OP_BEET")
	private Boolean opBeet;
	
	@Column(name="OP_HISTORY")
	private Boolean opHistory;
	
	@Column(name="OP_ADDCARD")
	private Boolean opAddCard;
	
	@Column(name="OP_FINANCIAL")
	private Boolean opFinancial;
	
	@Column(name="LOGO")
	private String logo;
	
	@Column(name="OP_RECOVERPIN")
	private Boolean opRecoverPin;
	
	@Column(name="RECOVERPIN_LABEL")
	private String recoverPinLabel;
	
	@Column(name="OP_PAY")
	private Boolean opPay;
	
	@Column(name="OP_TRANSFER_CODE")
	private Boolean opTransferCode;
	
	@Column(name="OP_RECARGA_TAE")
	private Boolean opRecargaTae;
	
	@Column(name="OP_CLABE")
	private Boolean opClabe;
	
	@Column(name="URL_PORTAL")
	private String urlPortal;
	
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getIsoCode() {
		return isoCode;
	}
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getEmailDomains() {
		return emailDomains;
	}
	public void setEmailDomains(String emailDomains) {
		this.emailDomains = emailDomains;
	}
	public String getUrlRegister() {
		return urlRegister;
	}
	public void setUrlRegister(String urlRegister) {
		this.urlRegister = urlRegister;
	}
	public String getPackageMode() {
		return packageMode;
	}
	public void setPackageMode(String packageMode) {
		this.packageMode = packageMode;
	}
	public String getPackageAccount() {
		return packageAccount;
	}
	public void setPackageAccount(String packageAccount) {
		this.packageAccount = packageAccount;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUrlTerms() {
		return urlTerms;
	}
	public void setUrlTerms(String urlTerms) {
		this.urlTerms = urlTerms;
	}
	public String getCallCenter() {
		return callCenter;
	}
	public void setCallCenter(String callCenter) {
		this.callCenter = callCenter;
	}
	public String getCallCenterText() {
		return callCenterText;
	}
	public void setCallCenterText(String callCenterText) {
		this.callCenterText = callCenterText;
	}
	public String getCallCenterLabel() {
		return callCenterLabel;
	}
	public void setCallCenterLabel(String callCenterLabel) {
		this.callCenterLabel = callCenterLabel;
	}
	public String getBankCodeRegister() {
		return bankCodeRegister;
	}
	public void setBankCodeRegister(String bankCodeRegister) {
		this.bankCodeRegister = bankCodeRegister;
	}
	public String getWordingSmsTransfer() {
		return wordingSmsTransfer;
	}
	public void setWordingSmsTransfer(String wordingSmsTransfer) {
		this.wordingSmsTransfer = wordingSmsTransfer;
	}
	public Date getModificationDate() {
		return modificationDate;
	}
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	public Boolean getOpTransfer() {
		return opTransfer;
	}
	public void setOpTransfer(Boolean opTransfer) {
		this.opTransfer = opTransfer;
	}
	public Boolean getOpCash() {
		return opCash;
	}
	public void setOpCash(Boolean opCash) {
		this.opCash = opCash;
	}
	public Boolean getOpFounding() {
		return opFounding;
	}
	public void setOpFounding(Boolean opFounding) {
		this.opFounding = opFounding;
	}
	public Boolean getOpCharge() {
		return opCharge;
	}
	public void setOpCharge(Boolean opCharge) {
		this.opCharge = opCharge;
	}
	public Boolean getOpBeet() {
		return opBeet;
	}
	public void setOpBeet(Boolean opBeet) {
		this.opBeet = opBeet;
	}
	public Boolean getOpHistory() {
		return opHistory;
	}
	public void setOpHistory(Boolean opHistory) {
		this.opHistory = opHistory;
	}
	public Boolean getOpAddCard() {
		return opAddCard;
	}
	public void setOpAddCard(Boolean opAddCard) {
		this.opAddCard = opAddCard;
	}
	public Boolean getOpFinancial() {
		return opFinancial;
	}
	public void setOpFinancial(Boolean opFinancial) {
		this.opFinancial = opFinancial;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public Boolean getOpRecoverPin() {
		return opRecoverPin;
	}
	public void setOpRecoverPin(Boolean opRecoverPin) {
		this.opRecoverPin = opRecoverPin;
	}
	public String getRecoverPinLabel() {
		return recoverPinLabel;
	}
	public void setRecoverPinLabel(String recoverPinLabel) {
		this.recoverPinLabel = recoverPinLabel;
	}
	public Boolean getOpPay() {
		return opPay;
	}
	public void setOpPay(Boolean opPay) {
		this.opPay = opPay;
	}
	public Boolean getOpTransferCode() {
		return opTransferCode;
	}
	public void setOpTransferCode(Boolean opTransferCode) {
		this.opTransferCode = opTransferCode;
	}
	public Boolean getOpRecargaTae() {
		return opRecargaTae;
	}
	public void setOpRecargaTae(Boolean opRecargaTae) {
		this.opRecargaTae = opRecargaTae;
	}
	public Boolean getOpClabe() {
		return opClabe;
	}
	public void setOpClabe(Boolean opClabe) {
		this.opClabe = opClabe;
	}
	public String getUrlPortal() {
		return urlPortal;
	}
	public void setUrlPortal(String urlPortal) {
		this.urlPortal = urlPortal;
	}

	

}
