package com.globalhitss.t1payment.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.globalhitss.t1payment.entity.MerchantClaroPay;

/**
 *
 * @author agonzalz
 */
@Repository
public interface MerchantClaroPayRepository extends JpaRepository<MerchantClaroPay, String> {

	MerchantClaroPay findByMerchantId(String merchantId);

}
