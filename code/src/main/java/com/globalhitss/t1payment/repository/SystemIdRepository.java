package com.globalhitss.t1payment.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.globalhitss.t1payment.entity.SystemIdEntity;

public interface SystemIdRepository extends JpaRepository<SystemIdEntity, Serializable> {
	
	@Query("SELECT s FROM SystemIdEntity s WHERE s.hostName =:hostName and s.module =:module")
	String getSystemIdByHostName(@Param(value="hostName") String hostName, @Param(value="module")Integer module);
	
	@Query("SELECT s FROM SystemIdEntity s WHERE s.hostName =:hostName and s.module =:module")
	SystemIdEntity getByHostNameAndModule(@Param(value="hostName")String hostName, @Param(value="module")Integer module);
	
	@Query("SELECT COUNT(s) FROM SystemIdEntity s WHERE s.module =?1")
	Integer count(Integer module);
	
	@Query("SELECT max(s.systemId) from SystemIdEntity s WHERE s.module =?1")
	Integer maxSystemIdToModule (Integer module);
}
