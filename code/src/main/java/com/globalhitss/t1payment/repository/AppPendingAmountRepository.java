package com.globalhitss.t1payment.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.globalhitss.t1payment.entity.AppPendingAmount;

@Repository
public interface AppPendingAmountRepository extends JpaRepository<AppPendingAmount, Serializable> {

	@Query("SELECT pa FROM AppPendingAmount pa WHERE pa.pendingAmountId =:id")
	AppPendingAmount getPendingAmountById(@Param(value = "id") Long id);
}
