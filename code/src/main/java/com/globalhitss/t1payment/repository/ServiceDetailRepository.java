package com.globalhitss.t1payment.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.globalhitss.t1payment.entity.ServiceDetail;

@Repository
public interface ServiceDetailRepository extends JpaRepository<ServiceDetail, Serializable> {
	@Query("SELECT sd FROM ServiceDetail sd WHERE code =:code")
	ServiceDetail getServiceDetailByCode(@Param(value="code") String code);
}
