package com.globalhitss.t1payment.vo.request;

public class T1PaymentServiceRequest extends GenericRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador del servicio
	 */
	private String transactionType;
	/**
	 * Monto de la transaccion
	 */
	private Double amount;
	/**
	 * Referencia del recibo de pago
	 */
	private String destAccountId;
	/**
	 * Descripcion del pago
	 */
	private String paymentDescription;
	/**
	 * Token de la tarjeta obtenido del sistema T1 Pagos
	 */
	private String cardToken;

	/**
	 * NIP que autoriza la operacion
	 */
	private String pin;

	private String destAccountIdType;
	private String productKey;

	/**
	 * Identificador del cliente registrado en sistema T1 Pagos
	 */
	private String clientIdT1;
	/**
	 * Identificador para comunicarse con el sistema T1 Pagos
	 */
	private String deviceFingerPrint;

	public T1PaymentServiceRequest() {
	}

	public T1PaymentServiceRequest(String transactionType, Double amount, String destAccountId,
			String paymentDescription, String cardToken, String pin, String destAccountIdType, String productKey,
			String clientIdT1, String deviceFingerPrint) {
		this.transactionType = transactionType;
		this.amount = amount;
		this.destAccountId = destAccountId;
		this.paymentDescription = paymentDescription;
		this.cardToken = cardToken;
		this.pin = pin;
		this.destAccountIdType = destAccountIdType;
		this.productKey = productKey;
		this.clientIdT1 = clientIdT1;
		this.deviceFingerPrint = deviceFingerPrint;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDestAccountId() {
		return destAccountId;
	}

	public void setDestAccountId(String destAccountId) {
		this.destAccountId = destAccountId;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public String getCardToken() {
		return cardToken;
	}

	public void setCardToken(String cardToken) {
		this.cardToken = cardToken;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getDestAccountIdType() {
		return destAccountIdType;
	}

	public void setDestAccountIdType(String destAccountIdType) {
		this.destAccountIdType = destAccountIdType;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getClientIdT1() {
		return clientIdT1;
	}

	public void setClientIdT1(String clientIdT1) {
		this.clientIdT1 = clientIdT1;
	}

	public String getDeviceFingerPrint() {
		return deviceFingerPrint;
	}

	public void setDeviceFingerPrint(String deviceFingerPrint) {
		this.deviceFingerPrint = deviceFingerPrint;
	}

	@Override
	public String toString() {
		return "T1PaymentServiceRequest [transactionType=" + transactionType + ", amount=" + amount + ", destAccountId="
				+ destAccountId + ", paymentDescription=" + paymentDescription + ", cardToken=" + cardToken + ", pin="
				+ pin + ", destAccountIdType=" + destAccountIdType + ", productKey=" + productKey + ", clientIdT1="
				+ clientIdT1 + ", deviceFingerPrint=" + deviceFingerPrint + "]";
	}

}
