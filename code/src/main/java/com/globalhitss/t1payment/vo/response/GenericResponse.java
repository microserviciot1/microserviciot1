package com.globalhitss.t1payment.vo.response;

import java.io.Serializable;

/**
 * **
 * Parent class from which all repsonse type objects must extend *
 **/
 
public class GenericResponse implements Serializable {
	
	private static final long serialVersionUID = -2853724452178276855L;

	private int responseCode;
	private String responseMessage;
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	private String responseSubject;
	private String timeStamp;
	private String externalId;
	private String transId;
	private int messageType;
	
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseSubject() {
		return responseSubject;
	}
	public void setResponseSubject(String responseSubject) {
		this.responseSubject = responseSubject;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = Long.toString(timeStamp);
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	
	
}
