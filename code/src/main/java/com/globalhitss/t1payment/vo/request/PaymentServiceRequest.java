package com.globalhitss.t1payment.vo.request;

public class PaymentServiceRequest extends GenericRequest {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2358785996681493728L;
	private Double amount;
	private String transactionType;
	private String destAccountId;
	private String destAccountIdType;
	private Boolean pendingAmountOrigin;
	private Long pendingAmountId;
	private String productKey;
	private String pin;
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getDestAccountId() {
		return destAccountId;
	}
	public void setDestAccountId(String destAccountId) {
		this.destAccountId = destAccountId;
	}
	public String getDestAccountIdType() {
		return destAccountIdType;
	}
	public void setDestAccountIdType(String destAccountIdType) {
		this.destAccountIdType = destAccountIdType;
	}
	public Boolean getPendingAmountOrigin() {
		return pendingAmountOrigin;
	}
	public void setPendingAmountOrigin(Boolean pendingAmountOrigin) {
		this.pendingAmountOrigin = pendingAmountOrigin;
	}
	public Long getPendingAmountId() {
		return pendingAmountId;
	}
	public void setPendingAmountId(Long pendingAmountId) {
		this.pendingAmountId = pendingAmountId;
	}
	public String getProductKey() {
		return productKey;
	}
	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	
	
}
