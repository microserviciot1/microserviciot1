package com.globalhitss.t1payment.vo;

import java.util.Date;

public class LogTransaccion {

	private Date inicioTransaccion;
	private Date finTransaccion;
	private Long mensajeId;
	private String envia;
	private String recibe;
	private String canal;
	private Integer codigoRespuesta;
	private String descripcionRespuesta;
	private String externalId;
	private String longitude;
	private String latitude;

	public Date getInicioTransaccion() {
		return inicioTransaccion;
	}

	public void setInicioTransaccion(Date inicioTransaccion) {
		this.inicioTransaccion = inicioTransaccion;
	}

	public Date getFinTransaccion() {
		return finTransaccion;
	}

	public void setFinTransaccion(Date finTransaccion) {
		this.finTransaccion = finTransaccion;
	}

	public Long getMensajeId() {
		return mensajeId;
	}

	public void setMensajeId(Long mensajeId) {
		this.mensajeId = mensajeId;
	}

	public String getEnvia() {
		return envia;
	}

	public void setEnvia(String envia) {
		this.envia = envia;
	}

	public String getRecibe() {
		return recibe;
	}

	public void setRecibe(String recibe) {
		this.recibe = recibe;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public Integer getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(Integer codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "LogTransaccion [inicioTransaccion=" + inicioTransaccion + ", finTransaccion=" + finTransaccion
				+ ", mensajeId=" + mensajeId + ", envia=" + envia + ", recibe=" + recibe + ", canal=" + canal
				+ ", codigoRespuesta=" + codigoRespuesta + ", descripcionRespuesta=" + descripcionRespuesta
				+ ", externalId=" + externalId + ", longitude=" + longitude + ", latitude=" + latitude + "]";
	}

}
