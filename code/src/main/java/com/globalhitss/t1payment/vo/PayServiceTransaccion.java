package com.globalhitss.t1payment.vo;

public class PayServiceTransaccion extends LogTransaccion {

	private String montoPendienteId;
	private String monto;

	public String getMontoPendienteId() {
		return montoPendienteId;
	}

	public void setMontoPendienteId(String montoPendienteId) {
		this.montoPendienteId = montoPendienteId;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

}
