package com.globalhitss.t1payment.vo.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class T1CommercePaymentRequest extends GenericRequest {

    private static final long serialVersionUID = 7478014643915321938L;

    @NotEmpty(message = "{validation.msg.mandatory}")
    private String amount;
    @NotEmpty(message = "{validation.msg.mandatory}")
    private String cardToken;
    @Pattern(regexp = "(^[a-zA-Z0-9\\s]{0,40}$)?", message = "{validation.msg.alphanum-ref.max-length}")
    private String alphanumericReference;
    @Pattern(regexp = "(^[a-zA-Z0-9\\s]{0,100}$)?", message = "{validation.msg.description.max-length}")
    private String description;
    @NotEmpty(message = "{validation.msg.mandatory}")
    private String claroUserId;
    @NotEmpty(message = "{validation.msg.mandatory}")
    private String merchantId;
    @NotEmpty(message = "{validation.msg.mandatory}")
    private String clientId;
    @NotEmpty(message = "{validation.msg.mandatory}")
    private String fingerPrint;

    public T1CommercePaymentRequest() {
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlphanumericReference() {
        return alphanumericReference;
    }

    public void setAlphanumericReference(String alphanumericReference) {
        this.alphanumericReference = alphanumericReference;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getClaroUserId() {
        return claroUserId;
    }

    public void setClaroUserId(String claroUserId) {
        this.claroUserId = claroUserId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getFingerPrint() {
        return fingerPrint;
    }

    public void setFingerPrint(String fingerPrint) {
        this.fingerPrint = fingerPrint;
    }

    @Override
    public String toString() {
        return "T1CommercePaymentRequest{" +
                "amount='" + amount + '\'' +
                ", cardToken='" + cardToken + '\'' +
                ", alphanumericReference='" + alphanumericReference + '\'' +
                ", description='" + description + '\'' +
                ", claroUserId='" + claroUserId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", fingerPrint='" + fingerPrint + '\'' +
                "} " + super.toString();
    }
}
