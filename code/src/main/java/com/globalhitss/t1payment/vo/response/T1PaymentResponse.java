package com.globalhitss.t1payment.vo.response;

import java.util.List;

public class T1PaymentResponse extends GenericResponse {

	private static final long serialVersionUID = -8453243727034455585L;

	private List<String> dataValidationErrors;

	public List<String> getDataValidationErrors() {
		return dataValidationErrors;
	}

	public void setDataValidationErrors(List<String> dataValidationErrors) {
		this.dataValidationErrors = dataValidationErrors;
	}

}
