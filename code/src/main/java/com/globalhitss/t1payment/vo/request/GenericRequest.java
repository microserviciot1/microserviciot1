package com.globalhitss.t1payment.vo.request;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


/**
 * Parent class from which all request type objects must extend *
 **/
public class GenericRequest implements Serializable{

	private static final long serialVersionUID = -6191436137014120710L;
	private String authorization;
	private String authorizationType;
	private String appVersion;
	private String externalId;
	private String latitude;
	private String longitude;
	@Pattern(regexp = "[0-9]{10}", message = "{validation.msg.mandatory.msisdn.length}")
	private String msisdn;
	@NotEmpty(message = "{validation.msg.mandatory}")
	private String consumerKey;
	
	
	
	public String getAuthorization() {
		return authorization;
	}
	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}
	public String getAuthorizationType() {
		return authorizationType;
	}
	public void setAuthorizationType(String authorizationType) {
		this.authorizationType = authorizationType;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getConsumerKey() {
		return consumerKey;
	}
	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}
}
