package com.globalhitss.t1payment;

import com.globalhitss.t1payment.exception.AppWsException;
import com.globalhitss.t1payment.model.SystemIdModel;
import com.globalhitss.t1payment.service.SystemIdAllocator;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;



@SpringBootApplication
@EnableAspectJAutoProxy
@EnableJms
@EnableSwagger2
@PropertySources(value = {
        @PropertySource(value = "classpath:messages.properties")
})
public class T1PaymentApplication {

	private static final Log LOG = LogFactory.getLog(T1PaymentApplication.class);

	@Value("${broker.url}")
	private String brokerUrl;

	@Autowired
	private SystemIdAllocator systemIdAllocator;

	@Autowired
	private ConfigurableApplicationContext appContext;

	public static void main(String[] args) {
		SpringApplication.run(T1PaymentApplication.class, args);
	}

	@Bean(destroyMethod = "stop")
	public PooledConnectionFactory jmsConnectionFactory() {
		PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
		ActiveMQConnectionFactory amqConnectionFactory = new ActiveMQConnectionFactory(brokerUrl);
		amqConnectionFactory.setUseAsyncSend(true);
		amqConnectionFactory.setOptimizeAcknowledge(true);
		amqConnectionFactory.setAlwaysSessionAsync(true);
		amqConnectionFactory.setCloseTimeout(100);
		amqConnectionFactory.setTrustAllPackages(true);
		pooledConnectionFactory.setConnectionFactory(amqConnectionFactory);
		return pooledConnectionFactory;
	}

	@Bean
	public JmsListenerContainerFactory<? extends MessageListenerContainer> myFactory(DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(jmsConnectionFactory());
        return factory;
	}

	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}

	@Bean
	public ApiInfo apiInfo() {
		return new ApiInfo("rest-transactional", "Transactional operations to claropay", "1.0-demo",
				"https://www.claroshop.com/terminos-y-condiciones/",
				new Contact("CITI value in Real Time", "url", "test@citi.com.mx"), "license", "licenseUrl",
				Collections.emptyList());
	}
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
				.tags(new Tag("services", "Services related"), new Tag("accounts", "accounts related"),
						new Tag("payments", "payments related"))
				.select().apis(RequestHandlerSelectors.basePackage("com.globalhitss.transactionalws.rest")).build();
	}

	@Bean
	@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
	public SystemIdModel systemIdModel() {
		SystemIdModel systemIdModel = new SystemIdModel();
		LOG.info("generating systemId to this component: " + this.getClass());
		try {
			systemIdModel = systemIdAllocator.getSystemIdByHostNameAndModule(InetAddress.getLocalHost().getHostName(), 1);
			if (systemIdModel != null) {
				LOG.info("cat_systemid: " + systemIdModel.toString());
			} else {
				LOG.error("CRITICAL ERROR: could not get systemId for this instance..");
				appContext.getBean(this.getClass());
				appContext.close();
			}
		} catch (UnknownHostException e) {
			LOG.error("Could not get hostname: " + e.getMessage(), e);
			appContext.getBean(this.getClass());
			appContext.close();
		} catch (AppWsException e) {
			LOG.error(e.getMessage());
			appContext.getBean(this.getClass());
			appContext.close();
		}
		return systemIdModel;
	}

	@Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }


}
