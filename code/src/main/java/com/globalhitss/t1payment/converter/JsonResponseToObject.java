package com.globalhitss.t1payment.converter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalhitss.integrationws.payload.response.GatewayResponseMessage;
import com.globalhitss.t1payment.exception.AppWsException;

@Component
public class JsonResponseToObject {

	public GatewayResponseMessage jsonToGatewayResponseMessage(String json) throws AppWsException {
		if (json == null) {
			throw new AppWsException("getting a null json from gateway", -1001,
					"null received in response from gateway");
		}
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, GatewayResponseMessage.class);
		} catch (Exception e) {
			throw new AppWsException("The json could not be deserialized to GatewayResponseMessage", -1001,
					"malformed jsonResponse or invaild");
		}

	}
}
