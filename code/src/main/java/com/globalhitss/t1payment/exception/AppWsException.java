package com.globalhitss.t1payment.exception;

public class AppWsException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2142942995972556325L;
	private int code;
	private String reason;
	
	public AppWsException(String message, int code, String reason) {
		super(message);
		this.code = code;
		this.reason = reason;
	}

	public AppWsException(String message, Throwable cause, int code, String reason) {
		super(message, cause);
		this.code = code;
		this.reason = reason;
	}
	
	public AppWsException(int code){
		super();
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}